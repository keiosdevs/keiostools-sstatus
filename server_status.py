#!/usr/bin/python3
import os, sys
import configparser
import subprocess
import re

config = configparser.ConfigParser()
config.read('/etc/server_status.cnf')

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    
print (bcolors.OKBLUE + "Services Status" + bcolors.ENDC)
print ("")
services_list = []
commands_list = []
output = subprocess.getoutput('ps ax')

for item in config['choicelist']:
    if (config['choicelist'][item]) == '1':
        services_list.append(item)
for item in config['services']:
    if (config['choicelist'][item]) == '1':
        commands_list.append(item)
for item in commands_list:
    if item in services_list:
        cmd_name = str(config['services'][item]).replace("'","")
        if cmd_name in output:
            print ('[' + bcolors.OKBLUE + 'OK' + bcolors.ENDC + '] ' + item + ' is running.' )
        else:
            if cmd_name == 'apache2':
                print ('[' + bcolors.WARNING + 'GOOD' + bcolors.ENDC + '] ' + item + ' is not running.' )
            else:
                print ('[' + bcolors.FAIL + 'FAIL' + bcolors.ENDC + '] ' + item + ' is not running.' )
print ("")




